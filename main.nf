#!/usr/bin/env nextflow

nextflow.enable.dsl=2

// Define parameters with default values
params.source = ''
params.target = ''
params.rsyncParams = '-azrvL'

process RSYNC {

    publishDir "rsync/"+new Date().format("yyyy-MM-dd-HHmmss"), mode: 'copy'

    cpus 4
    memory 4.GB

    cpus = { 4 * task.attempt }
    memory = { 4.GB * task.attempt }
  
    errorStrategy = 'retry'
    maxRetries = 2

    input:
    val source
    val target
    val rsyncParams

    output:
    path 'rsync.txt', emit: report

    script:
    """
    rsync --progress ${rsyncParams}  ${source} ${target} > rsync.txt  2>/dev/null
    tail -n 2 rsync.txt
    """
}


workflow {
    RSYNC(params.source, params.target, params.rsyncParams)
}
